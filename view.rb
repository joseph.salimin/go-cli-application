require './helper'

# View of the app
# Print everything from controller to terminal
# Good UI Tho

class GoCliView

	def print_logo(user)
		clear_screen
		puts " ______     ______                ______     __         __ "   
		puts "/\\  ___\\   /\\  __ \\    _______   /\\  ___\\   /\\ \\       /\\ \\ "    
		puts "\\ \\ \\__ \\  \\ \\ \\/\\ \\  /\\______\\  \\ \\ \\____  \\ \\ \\____  \\ \\ \\ " 
		puts " \\ \\_____\\  \\ \\_____\\ \\/______/   \\ \\_____\\  \\ \\_____\\  \\ \\_\\ "
		puts "  \\/_____/   \\/_____/              \\/_____/   \\/_____/   \\/_/ "
		puts ""
		puts ""
		puts "Welcome to Go-CLI Application, #{user}!"
		puts ""
		puts ""
	end

	def print_strip(size)
		for i in 0...size do 
			print "+---"
		end
		puts '+'
	end

	def timedown
		time = 3
		puts "Application will run in..."
		while time > 0 do
			puts time
			sleep(1)
			time -= 1
		end
	end

	def print_choices
		puts "What do you want to do?"
		puts "1. Show map"
		puts "2. Order Go-Ride"
		puts "3. View History"
		puts "4. Exit"
		puts "Your input:"
		print "> "
	end

	def print_continue
		puts "Press Enter to continue"
		gets.chomp
	end

	def print_map(map)
		map.each do |row|
			print_strip(map.length)
			print "| "
			row.each do |col|
				print col.symbol
				print " | "
			end
			puts ''
		end
		print_strip(map.length)
	end

	def print_order_confirmation(order)
		puts "Your order information:"
		puts "User: " + order.user
		puts "Your location: " + "(" + order.start_loc[:x].to_s + ", " + order.start_loc[:y].to_s + ")"
		puts "Destination location: " + "(" + order.end_loc[:x].to_s + ", " + order.end_loc[:y].to_s + ")"
		puts "Driver: " + order.driver
		puts "Driver location: " + "(" + order.driver_loc[:x].to_s + ", " + order.driver_loc[:y].to_s + ")"
		puts "Route: " + order.route.join(" -> ")
		puts "Price: " + order.price.to_s
		puts ""
		puts "Do you want to order it? (1 for order, else cancel order)"
	end

	def print_order_canceled
		puts "You have canceled your order"
	end

	def print_order_saved
		puts "Your have ordered Go-CLI. Thank you for using our applications :)"
		puts ""
	end

	def print_order_history(orders)
		puts ""
		puts "YOUR ORDERS"
		puts "-----------"
		i = 1
		orders.each do |order|
			puts "Order-" + i.to_s
			puts "Time: " + order.time
			puts "User: " + order.user
			puts "Your location: " + order.start_loc
			puts "Destination location: " + order.end_loc
			puts "Driver: " + order.driver
			puts "Driver location: " + order.driver_loc
			puts "Route: " + order.route
			puts "Price: " + order.price
			puts ""
			i += 1
		end
	end

	def print_goodbye
		puts "Thank you for using Go-CLI Application!!"
	end

	def print_wrong_choice
		puts "Your choice can't be determined by Go-Cli Application! Try Again!"
	end

end