require './model'
require './helper'
require './config'

# This the the builder to create order
# Based on builder pattern
# Creating order step by step

class OrderBuilder

	def initialize
		@order = Order.new
	end

	def set_start_location(coor_x, coor_y)
		@order.start_loc = {x: coor_x, y: coor_y}
	end

	def set_user(user)
		@order.user = user
	end

	def set_end_location(map)
		puts "Set location you want to go!"
		while true
			print "Coordinate X: "
			coor_x = Integer(gets) rescue false
			print "Coordinate Y: "
			coor_y = Integer(gets) rescue false
			if coor_x and coor_x >= 0 and coor_x < map.size
				if coor_y and coor_y >= 0 and coor_y < map.size
					break if not map.is_grid_occupied(coor_x, coor_y)
				end
			end
		end
		@order.end_loc = {x: coor_x, y: coor_y}
	end

	# Get nearest driver
	def get_nearest_driver(user, drivers)
		# Initialize
		distance = 999999
		# Get user_loc
		user_loc = {x: user.x, y: user.y}
		# Find nearest driver by iterating every driver
		drivers.each do |driver|
			driver_loc = {x: driver.x, y: driver.y}
			temp_distance = get_distance(user_loc, driver_loc)
			# Check if N driver is closer than other drivers
			if temp_distance < distance
				distance = temp_distance
				@order.driver = driver.username
				@order.driver_loc = driver_loc
			end
		end
	end

	# Get the route
	def get_route
		route = []
		route.push("start at (" + @order.start_loc[:x].to_s + ", " + @order.start_loc[:y].to_s + ")")
		choice = [:horizontal, :vertical].sample
		# 1st case
		if @order.start_loc.fetch(:x) == @order.end_loc.fetch(:x) \
		or @order.start_loc.fetch(:y) == @order.end_loc.fetch(:y)
			choice = :straight
		end
		# 2nd case
		if choice == :horizontal
			route.push("go to (" + @order.end_loc[:x].to_s + ", " + @order.start_loc[:y].to_s + ")")
			route.push("turn "+ get_direction_horizontal(@order.start_loc, @order.end_loc))
		elsif choice == :vertical		
			route.push("go to (" + @order.start_loc[:x].to_s  + ", " + @order.end_loc[:y].to_s + ")")
			route.push("turn "+ get_direction_vertical(@order.start_loc, @order.end_loc))		
		end
		route.push("go to (" + @order.end_loc[:x].to_s + ", " + @order.end_loc[:y].to_s + ")")
		route.push("finish at (" + @order.end_loc[:x].to_s + ", " + @order.end_loc[:y].to_s + ")")
		@order.route = route
	end

	def set_price
		# Get distance
		distance = get_distance(@order.start_loc, @order.end_loc)
		# Get hour
		hour = Time.new.hour
		# Set price
		if hour >= HIGH_START and hour <= HIGH_END
			@order.price = HIGH_PRICE * distance
		else
			@order.price = NORMAL_PRICE * distance
		end
	end
	
	def return_order
		@order
	end

	# Create order from file
	# To be written only
	# Have different attributes from another method
	def self.create_order_from_file(file)
		order_file = read_file(file)
		# Create order from file
		order = Order.new
		order.user = (order_file[0].split(" ", 2))[1]
		order.start_loc = (order_file[1].split(" ", 2))[1]
		order.end_loc = (order_file[2].split(" ", 2))[1]
		order.driver = (order_file[3].split(" ", 2))[1]
		order.driver_loc = (order_file[4].split(" ", 2))[1]
		order.route = (order_file[5].split(" ", 2))[1]
		order.price = (order_file[6].split(" ", 2))[1]
		class << order
			attr_accessor :time
		end
		order.time = (order_file[7].split(" ", 2))[1]
		# Return
		order
	end

end