# Import
require 'optparse'

def clear_screen
	# Clear terminal to make it beautiful
	if RUBY_PLATFORM =~ /win32|win64|\.NET|windows|cygwin|mingw32/i
		system('cls')
	else
		system('clear')
	end
end

def get_arguments
	# Initialize options
	options = {}
	# Create parser
	opt_parser = OptionParser.new do |opt|
		opt.banner = "Welcome! Here is the command line for the application in case you need it"

		opt.on("-s", "--size [Size]","The size of the map") do |size|
			options[:size] = size
		end

		opt.on("-x", "--coorX [Coordinate X]", "Your coordinate in x") do |x|
			options[:x] = x
		end

		opt.on("-y", "--coorY [Coordinate Y]", "Your coordinate in y") do |y|
			options[:y] = y
		end

		opt.on("-f", "--file [Filename with right directory]", "Your file name (If you put filename, other option will be disabled)") do |filename|
			options[:file] = filename
		end

		opt.on("-h", "--help", "Give information about options") do
			puts opt_parser
			exit 1
		end
	end
	# Parse the option
	opt_parser.parse!
	# Show information
	puts opt_parser
	# Return options
	options
end 

def read_input_file(filename)
	# Initialize variable
	drivers = []
	coor_user = []
	size = 20
	begin
		# Read the file
		File.open("input/" + filename + ".txt", "r") do |f|
			f.each_line do |line|
				line_a = line.split(" ")
				# Just my formatting
				if line_a[0] == "MAP_SIZE"
					size = line_a[1].to_i
				elsif line_a[0] == "USER_COORDINATE"
					coor_user.push(line_a[1].to_i)
					coor_user.push(line_a[2].to_i)
				elsif line_a[0] == "DRIVER"
					drivers.push([line_a[1], line_a[2].to_i, line_a[3].to_i])
				else
					puts "Error reading file"
					exit 1
				end		
			end
		end
	rescue => err
		puts "Error reading file"
		puts "Exception: #{err}"
	end
	[size, coor_user, drivers]
end

def read_user_file
	File.open('input/user.txt') {|f| f.readline.split(":")[1].strip!}
end

def read_file(file)
	lines = []
	File.open(file, "r") do |f|
		f.each_line do |line|
			lines.push(line)
		end
	end
	lines
end

def get_distance(loc_1, loc_2)
	dist_x = loc_1.fetch(:x) - loc_2.fetch(:x)
	dist_y = loc_1.fetch(:y) - loc_2.fetch(:y)
	# Return
	dist_x.abs + dist_y.abs
end

# Direction needed when move horizontally at first
def get_direction_horizontal(start_loc, end_loc)
	direction = ""
	if start_loc[:x] < end_loc[:x]
		if start_loc[:y] > end_loc[:y]
			direction = "left"
		else
			direction = "right"
		end
	else
		if start_loc[:y] > end_loc[:y]
			direction = "right"
		else
			direction = "left"
		end
	end
	# Return direction
	direction
end

# Direction needed when move vertically at first
def get_direction_vertical(start_loc, end_loc)
	direction = ""
	if start_loc[:y] < end_loc[:y]
		if start_loc[:x] < end_loc[:x]
			direction = "left"
		else
			direction = "right"
		end
	else
		if start_loc[:x] < end_loc[:x]
			direction = "right"
		else
			direction = "left"
		end
	end
	# Return direction
	direction
end