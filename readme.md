# Software Engineering Academy
# Compfest X - GOJEK
# Second Stage Final Assignment

## GO-CLI Application!


### Descriptions

A very simple CLI Program which illustrates GOJEK in a CLI Program (of course the program is very simple). In this application, you can try to create order, get your driver, and arrive at your destination!

### Before Using..

1. Programming language : ruby 2.5.1p57 (2018-03-29 revision 63029) [x86_64-linux]

2. Operating System : Linux (TESTED) or Windows (Not tested). To try it better using Linux Ubuntu

### Command line or Options

When running this program, you can actually add option to the program, here is the list of options you can add

```
  -s, --size [Size]                The size of the map
  -x, --coorX [Coordinate X]       Your coordinate in x
  -y, --coorY [Coordinate Y]       Your coordinate in y
  -f [Filename with right directory],
      --file                       Your file name (If you put filename, other option will be disabled)
  -h, --help                       Give information about options
```

For example
```
  ruby go-cli.rb --file input 
  ruby go-cli.rb -s 10 -x 5 -y 10
```

### Assumption

I assume that people who runs my program is not naughty and like to input what my program can't read. 
I assume that every person runs my program give the input with right format.
I also assume that the file read by my program always be in correct format as I have created before.
I also create the map as simple as the description says - no obstacles or whatever it is to be.

### My Design -> MVC + Builder

I create this simple program with MVC design pattern (Model, View, Controller) because I think this is the best pattern for this simple program.
MVC also is very famous pattern used in applications and it can dwells with many types of application, from the simple one to complicated one.
That means that there are controller which control how the program works, the flow of it, etc. 
There are also some models which is the object that will be read, change by the controller.
And last, there is view which functions as the output given by controller and shown to the user with good UI (even though it is only shown in the terminal)

- Controller

```
  This is the controller of the program and it controls how the program will work. We can actually say this is the brain of the program.
  The controller will control the flow of the data / the model and then give the output to the View to be shown to the user.
  I also put the main loop of the program in the controller where user will be given some choices they can choose.
  Whatever choice made, the controller will read it and then execute the given command with some instruction that I have created before.
  The controller I made will generate the application first with the options the user gave (it can be read from input file, or size given by user, etc.).
  Then it will show the logo and then move to the main loop where user can choose the commands until they choose 'Exit' command from the program.
```
- View

```
  The main function of View is to show user what the program has executed and the results it has created.
  The View has some methods, which is print_logo, print timedown, show choices the user can give, show order confirmation show history of order show map, etc.
  View can only be run by controller. 
```
- Model

```
  The base model which is needed by controller to read, write, etc. The model can be shown as the material which will be processed by the controller.
  In this program, there are some models that I needed, that is Grid, Map which contains the Grid, User model, Driver model, and Order Model.
```

Besides MVC, I also use Builder pattern to build and create the Order. I use builder pattern because I think the order will be created step by step.
It means that to create the order, I can't create it instantly, but need some steps and input from user. 
For example, when create the order, I need the input from the user, where the user wants to go (input). 
After that, the program need to find the drivers, calculate the price, etc and those things can't be done instantly, but step by step.
That's why I think builder pattern is good enough to be implemented in my program where this builder will be controlled by the controller.
I think this pattern will help me build the program easier and it proves me right.

### Features

1. Show map -> shows where the user is and the location of drivers nearby.
2. Order Go-Ride -> Create the order for the user
    - User input the destination
    - Find the nearest driver
    - Create the route
    - Calculate the price
    - User confirms
    - Saved to order history
3. View History -> Show order history corresponding to the username
4. Exit -> exit the program

### Route cases and Coordinate of the Map

I assume there are no obstacles in the program or map. So there are only two cases:
1. User go straight to the destination (same x coordinate or same y coordinate)
2. Move horizontal or vertical depending on the random generator, do the right or left turn, and the go straight to the destination

For the map, x is the reprentation of the column and y is the representation of the row. The matrix started at (0,0) at upper left of the map
Here is the example of the map:

```
+---+---+---+
|0,0|1,0|2,0|
+---+---+---+
|0,1|1,1|2,1|
+---+---+---+
|0,2|1,2|2,2|
+---+---+---+
```

### Screenshoot

![Picture 1](screenshots/1.png)

![Picture 2](screenshots/2.png)

![Picture 3](screenshots/3.png)

![Picture 4](screenshots/4.png)

### Author

##### Name :Joseph Salimin
##### Website : [josephsalimin.com](www.josephsalimin.com)